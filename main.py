#!/bin/env python

"""
This script fetches the workers of DB2Qs and prints them in JSON format.

Usage:

./main 

JSON to be printed:
{
    "success": true|false,
    "response": 
}

There is a mandatory conf.json file under ./conf/ which has this form:

{
    "datacenters": [
        {
            "datacenter": <datacenter_name>,
            "servers": [<list_of_servers>]
        },
        {... <another_datacenter> ...}
    ],
    "directories": [<list_of_directories>]
}

- list_of_servers: servers to look into for db2qs (should be the servers 
    where the db2q processes are configured/running).
- list_of_directories: directories the config files for db2qs redide in.

JSON to be printed out:

{
    "success": true|false,
    "result": [
        {
            "name": <datacenter_name>,
            "servers": [
                {
                    "name": <server_name>,
                    "files": [
                        {
                            "active": true|false,
                            "path": <path2config_file>,
                            "used_by": [list_apps_using_this_config_file],
                            "name": <file_name>,
                            "instances": [
                                "rows": <workers>,
                                "enabled": true|false,
                                "name": <instance_name>,
                                "cycle": <milliseconds>
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}


Please note the final result from checking processes depends on the 
applications.ALL file under /home/weblogic in every server.
"""

import ast
import getopt
import json
from os import path
import sys

from multiprocessing.pool import ThreadPool as Pool

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
LOG_FILE = path.join(EXEC_DIR, 'logs', 'db2q_workers.log')
CONF_FILE = path.join(EXEC_DIR, 'conf', 'conf.json')
REMOTE_SCRIPT = path.join(EXEC_DIR, 'rmt', 'fetch_data.py')

# sys.path.append(EXEC_DIR)
sys.path.append(path.dirname(EXEC_DIR))
from pssapi.cmdexe import cmd
from pssapi.utils import conf, rest, threadsafe#, util
# from api.api import IntegProcManagerAPI

result = None

def _get_server_data(datacenter, server, username, directories):
    def _server_error(error):
        result[datacenter][server]['error'] = error
    try:
        stdoutput, stderror = cmd.pythonexec(username, server, REMOTE_SCRIPT, 
                                            True, *directories)
        # print("output: " + stdoutput)
        # print("error: " + stderror)
        stderr = stderror.strip()
        if stderr:
            _server_error(stderr)
        else:
            # print(stdoutput)
            stdout = ast.literal_eval(stdoutput)
            # stdout = json.loads(stdoutput)
            result[datacenter][server] = stdout
    except Exception as e:
        _server_error(str(e))

def _load_datacenter_data(datacenter, servers, directories):
    pool = Pool(len(servers))
    for server_conf in servers:
        server = server_conf['name']
        username = server_conf['username']
        result[datacenter][server] = {}
        pool.apply_async(_get_server_data, 
                        (datacenter, server, username, directories))
    pool.close()
    pool.join()

def _order_data(config_values):
    """Order by datacenter and server name, in the same order defined in the 
    original config file (config_values). Return final result dictionary.
    This function was necessary since order is important when printing the 
    data in the frontend. It wasn't possible to keep the order when fetching 
    the data because multithreading is being used and the result from every 
    server is received asynchronously.
    """
    # if config_values is None, then the configuration file couldn't be 
    # fetched and/or parsed as a valid JSON file
    # in this case, global result has the error to print
    if not config_values:
        return result
    ordered_result = []
    for datacenter in config_values['datacenters']:
        dc_name = datacenter['datacenter'].upper()
        dt = {
            'name': dc_name,
            'servers': []
        }
        for server in datacenter['servers']:
            serv_name = server['name']
            serv = {
                'name': serv_name,
                'files': result[dc_name][serv_name]
            }
            dt['servers'].append(serv)
        ordered_result.append(dt)
    return ordered_result

def _main():
    global result
    result = threadsafe.ThreadSafeDict()
    err_msg = None
    config_values = None
    try:
        config_values = conf.get_conf(CONF_FILE)
        directories = config_values['directories']
        for datacenter in config_values['datacenters']:
            datacenter_name = datacenter['datacenter'].upper()
            result[datacenter_name] = {}
            _load_datacenter_data(datacenter_name, datacenter['servers'], 
                                  directories)
    except KeyError as ke:
        err_msg = ("'{0}' is a required key in the conf file. Please " + 
                   "check documentation.").format(str(ke))
    except Exception as e:
        err_msg = "Exception ocurred: {0}".format(e)
    finally:
        if err_msg:
            result['error'] = err_msg
        rest.response(_order_data(config_values), success=(not err_msg))

if __name__ == "__main__":
    _main()
