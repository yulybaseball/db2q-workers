#!/bin/env python

"""
Script to be run remotely to fetch data related to DB2Q processes.

Usage:

./fetch_data.py <directory_1> [directory_2[, directory_3[, directory_n]]]

- directory: all directories to search into for DB2Q configuration files.

This script assumes there is an applications.ALL in /home/<username>/, where 
<username> is the username used to connect to the server this script is 
being executed in.
"""

import glob
from lxml import etree as ET
import os
import subprocess
import sys

PATTERN_CONF_FILES = "*config*.xml"
COMMAND_CAT_APPS_ALL = "cat applications.ALL | grep ':1' | cut -f2 -d:"
COMMAND_CAT_XML_CONF = "cat {xml_conf_file}"
# COMMAND_STATUS_PROC_V1 = "ps -efo pid,args | grep ' \\-D{proc} ' | grep -v grep"
# COMMAND_STATUS_PROC_V2 = "ps -efo pid,args | grep ' \\-Dsname={proc} ' | grep -v grep"
# # command to start process should include the 'cd' command because the 
# # the script to start process includes Java libraries which resides in the 
# # same directory
# COMMAND_GET_FILES = "cd {directory}; ls -l  *config*.xml"

def cmd(command):
    """Execute command and return the output."""
    p = subprocess.Popen(command, stdin=subprocess.PIPE, shell=True,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return p.communicate()

def _get_command_output(command):
    output, dummy = cmd(command)
    return output.strip()

def _file_in_application_path(db2q_conf_f, app_path):
    """Compare db2queue file complete path with application complete path.
    If they match, return True. False otherwise.
    """
    spl_db2q_conf_f = db2q_conf_f.split('/')
    spl_app_path = app_path.split('/')
    # assuming db2queue config file root path is in this form:
    # /<something>/<any>/<times>/config/<config_file_name.xml>
    db2q_conf_f_root = "/".join(spl_db2q_conf_f[:-2])
    # assuming application file root path is in this form:
    # /<something>/<any>/<times>/<application_file_name.sh>
    app_path_root = "/".join(spl_app_path[:-1])
    return db2q_conf_f_root == app_path_root

def _processes_using_conf_file(applications_all_file, db2q_conf_f):
    """Create and return a list containing the complete path of the processes 
    currently running in server passed as parameter, and using the file 
    db2q_conf_f.
    """
    command = ""
    # file_name = db2q_conf_f.split('/')[-1]
    # print(type(applications_all_file))
    file_name = os.path.basename(db2q_conf_f)
    for app_path in applications_all_file.split('\n'):
        if _file_in_application_path(db2q_conf_f, app_path):
            command += (app_path + " ")
    if command: # we have data to execute grep command
        command = ("grep -l /config/{0} {1}").\
                    format(file_name, command)
        result = _get_command_output(command)
        if result:
            result = result.split('\n')
            return [proc for proc in result if proc]
    return []

def _get_query_rows(query):
    ns_query = ''.join(query.lower().split())
    index = ns_query.rfind("rownum<")
    lessthan = 1
    sliceindex = len("rownum<")
    if index == -1:
        lessthan = 0
        sliceindex = len("rownum<=")
        index = ns_query.rfind("rownum<=")
        if index == -1:
            return ""
    realindex = index + sliceindex
    rowscount = "0"
    while realindex < len(ns_query) and ns_query[realindex].isdigit():
        rowscount += str(ns_query[realindex])
        realindex += 1
    rowscount = int(rowscount) - lessthan
    return rowscount

def _instances_in_file(xml_str):
    # get instances in file
    instances = []
    root = ET.fromstring(xml_str)
    for carrier in root.findall('./SystemProperties/Carrier'):
        instance = {
            'name': carrier.attrib['id'], 
            'enabled' : carrier.find('Instance').attrib['enabled'],
            'cycle': carrier.find('Instance').attrib['exectime'],
            'rows': _get_query_rows(carrier.find('SQLQuery').text),
        }
        instances.append(instance)
    return instances

def _main():
    server_data = []
    try:
        directories = sys.argv[1:]
        applications_all_file = _get_command_output(COMMAND_CAT_APPS_ALL)
        if not applications_all_file:
            raise Exception("applications.ALL file couldn't be fetched.")
        for directory in directories:
            conf_files = glob.glob(os.path.join(directory, PATTERN_CONF_FILES))
            for conf_file in conf_files:
                conf_file_content = _get_command_output(
                        COMMAND_CAT_XML_CONF.format(xml_conf_file=conf_file))
                if conf_file_content:
                    try:
                        # check file is in use
                        pucf = _processes_using_conf_file(applications_all_file, 
                                                        conf_file)
                        # get instances in file
                        instances = _instances_in_file(conf_file_content)
                        # append the file
                        server_data.append({
                            'name': os.path.basename(conf_file), 
                            'path': os.path.dirname(conf_file),
                            'active': bool(pucf), 'used_by': pucf, 
                            'instances': instances
                        })
                    except Exception as ie:
                        error = ("Exception while processing file '{0}': " + 
                                 "{1}").format(conf_file, str(ie))
                        raise Exception(error)
        print(server_data)
    except Exception as e:
        print({'error': str(e)})

if __name__ == "__main__":
    _main()
